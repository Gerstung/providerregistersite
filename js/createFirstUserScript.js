$(document).ready(function () {

    function inputValid(){
        if (fmailValid() && fpassValid() && fpassRepeatValid()){
            return true;
        }
        else{
            return false;
        }
    }

    function fmailValid (){
        var mailValid = validateEmail($('#mail').val());
        if (!mailValid) {
            $('#mail').addClass("badInputField");
            return(false);
        }
        else {
            $('#mail').removeClass("badInputField");
            return(true);
        }
    }

    function fpassValid(){
        var password = $('#password').val();

        var hasUpperCase = /[A-Z]/.test(password);
        var hasLowerCase = /[a-z]/.test(password);
        var hasNumbers = /\d/.test(password);
        var hasNonalphas = /\W/.test(password);
        if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3 || password.length < 8) {
            $('#password').addClass("badInputField");
            return(false);
        }
        else {
            $('#password').removeClass("badInputField");
            return(true);
        }
    }

    function fpassRepeatValid(){
        var password = $('#password').val();
        var passwordRepeat = $('#passwordRepeat').val();
        if (password !== passwordRepeat) {
            $('#passwordRepeat').addClass("badInputField");
            return(false);
        }
        else {
            $('#passwordRepeat').removeClass("badInputField");
            return(true);
        }
    }

    // Check Mail validity
    $('#mail').on('input', function(){
        fmailValid();
    });

    // Check password strength
    $('#password').on('input', function(){
        fpassValid();
    });

    // Check repeated password
    $('#passwordRepeat').on('input', function () {
        fpassRepeatValid();
    });

    // Button clicked
    $('#submitButton').on('click', function(){
        if (inputValid()){
            console.log("Everything good, lets create the user");
            $('#inputError').addClass("hide");
            var doc = {
                name: $('#name').val(),
                mail: $('#mail').val(),
                password: $('#password').val()
            }
            postJson(HOST + "/users/register", doc, function(err, res){
                if(err){
                    console.log(err);
                    $('#postError').removeClass("hide");
                }
                else{
                    console.log("Success");
                }
            });
        }
        else{
            $('#inputError').removeClass("hide");
        }
    });

});