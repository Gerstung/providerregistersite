"use strict";

function init(){
    var id = getUrlParameter('id');
    postJson("/provider/confirm", {"id": id}, function(err, res){
        $('#loader').addClass("hide");
        if (err){
            console.log("Error!!");
            window.location.href = ERRORPAGE;
        }
        else{
            window.location.href = 
            console.log("Successfully confirmed");
        }
    })
}

function redirect(){
    // On button click redirect user to a new user and details page
    window.location.href = "details.html?id=" + getUrlParameter('id');
}

init();