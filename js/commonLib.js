// v0.1.1

const HOST = "http://localhost:3001";
const ERRORPAGE = "error.html";

var postJson = function(url, obj, callback) {
    $.ajax({
        url: HOST + url,
        type: "POST",
        data: JSON.stringify(
            obj
        ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (e) {
            callback(null, e);
        },
        error: function (e) {
            callback(1, e)
        }
    });
}

var getJson = function(url, obj, callback){
	$.ajax({
        url: HOST + url,
        type: "GET",
        data: JSON.stringify(
            obj
        ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (e) {
            callback(null, e);
        },
        error: function (e) {
            callback(1, e)
        }
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

var validateEmail = function (email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}