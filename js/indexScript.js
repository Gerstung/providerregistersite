
$( document ).ready(function() {
    var input = document.getElementById('input');
    var inputMail = document.getElementById('inputMail');
    input.addEventListener('keydown', (e) => {
        if (e.key === 'Enter') {
            validateProvider(e);
        }
    });
    inputMail.addEventListener('keydown', (e) => {
        if (e.key === 'Enter') {
            validateProvider(e);
        }
    });
    
    function validateProvider(){
        var payload = {
            name: document.getElementById("input").value,
            mail: document.getElementById("inputMail").value
        }

        postJson("/provider/register", payload, function(err, res){
            if(err){
                console.log(res);
                window.location.href = ERRORPAGE;              
            }
            else{
                console.log(res);
                window.location.href = 'checkMails.html';
            }
        })
        
    }

});
