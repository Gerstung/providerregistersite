function readFields () {
    var obj = {
        "details": {
            "owner": $('#ownerInput').val(),
            "adress": {
                "street": $('#streetInput').val(),
                "number": $('#numberInput').val(),
                "postalcode": $('#postalInput').val(),
                "town": $('#townInput').val(),
                "country": $('#countryInput').val(),
                "additional": $('#additionalInput').val()
            },
            "tel": $('#telInput').val(),
            "business": $('#businessCategoryInput').val(),
            "slogan": $('#sloganInput').val()
        }
    }

    return obj;
}

function submit (){
    var payload = readFields();
    postJson("/provider/update", payload, function (err, res) {
        if (err) {
            console.log(res);
            window.location.href = ERRORPAGE;
        }
        else {
            console.log(res);
            $('#success').removeClass("hide");
        }
    })
}